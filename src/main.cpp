/*
 * Sample program for DocodeModem　mini and LTE-M modem
 * Copyright (c) 2021 Circuit Desgin,Inc
 * Released under the MIT license
 */

#include <docodemo.h>
#include <HL7800ModemClient.h>
#include "modem_setting.h"

#define USE_DEEPSLEEP

#ifdef USE_DEEPSLEEP
// if use deep sleep,USB will be disconnected...
#define DEBUGPRNT UartExternal
#else
#define DEBUGPRNT SerialDebug
#endif

DOCODEMO Dm = DOCODEMO();
HL7800Modem modemL;
HL7800ModemClient client(&modemL);

int writeUart(const uint8_t *data, uint32_t len)
{
  return UartModem.write(data, len);
}

int readUart(uint8_t *data, uint32_t len, uint32_t timeout)
{
  UartModem.setTimeout(timeout);
  return UartModem.readBytes(data, len);
}

bool modem_init()
{
  DEBUGPRNT.println("###initialize LTE-M");

  Dm.ModemPowerCtrl(ON);

  digitalWrite(RF_RESET_OUT, LOW);
  vTaskDelay(1);
  digitalWrite(RF_RESET_OUT, HIGH);

  digitalWrite(RF_WAKEUP_OUT, HIGH);

  if (!modemL.init(&writeUart, &readUart))
  {
    DEBUGPRNT.println("###init Error");
    return false;
  }

  DEBUGPRNT.println("###OK\n");

  DEBUGPRNT.println("###Start activate...");

  if (!modemL.activate(APN, USERNAME, PASSWORD))
  {
    DEBUGPRNT.println("###try again");
    if (!modemL.activate(APN, USERNAME, PASSWORD))
    {
      DEBUGPRNT.println("###Error");

      return false;
    }
  }
  DEBUGPRNT.println("###OK\n");

  char ip[16];
  if (modemL.getIPaddress(ip))
  {
    DEBUGPRNT.print("####ip: ");
    DEBUGPRNT.println(ip);
  }

  return true;
}

volatile bool alarmTriggered = true;
uint8_t count = 0;

void wakeup(void)
{
  alarmTriggered = true;
}

static const char *host = "hogehoge.com";
static int port = 3000;

TaskHandle_t handle_httpget_task;
static void httpget(void *pvParameters)
{
  while (1)
  {
    ulTaskNotifyTake(pdTRUE, (TickType_t)portMAX_DELAY);

    DEBUGPRNT.print("\n###start connection ");
    DEBUGPRNT.print(host);
    DEBUGPRNT.println(":" + String(port));

    if (!client.connect(host, port))
    {
      DEBUGPRNT.println("###error");
      continue;
    }

    String url = "/";

    DEBUGPRNT.println("###Requesting URL: " + url);

    // This will send the request to the server
    client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                 "Host: " + host + "\r\n" +
                 "Connection: keep-alive\r\n\r\n");

    // vTaskDelay(10);

    unsigned long timeout = millis();
    bool ackok = true;
    while (client.available() == 0)
    {
      if ((millis() - timeout) > 5000)
      {
        DEBUGPRNT.println(">>> Client Timeout !");
        client.stop();
        ackok = false;

        break;
      }
    }
    if (!ackok)
      continue;

    // Read all the lines of the reply from server and print them to Serial
    while (client.available())
    {
      String line = client.readStringUntil('\n');
      DEBUGPRNT.println(line);
    }

    int rssi = modemL.getRssi();
    DEBUGPRNT.println("###RSSI: " + String(rssi));
  }
}

TaskHandle_t Handle_main_task;
static void main_task(void *pvParameters)
{
  long elapstime = millis();

  DEBUGPRNT.println("###Main task start");
  Dm.begin();

  Dm.LedCtrl(GREEN_LED, ON);
  Dm.LedCtrl(RED_LED, OFF);

  Dm.exPowerCtrl(ON);
  Dm.exUartPowerCtrl(ON);

  UartModem.begin(115200);
  if (modem_init())
  {
    Dm.LedCtrl(RED_LED, OFF);
  }
  else
  {
    DEBUGPRNT.println("### try again");
    if (modem_init())
    {
      Dm.LedCtrl(RED_LED, OFF);
    }
  }

  // Wakeup every 60 sec.
  Dm.setRtcTimer(RtcTimerPeriod::SECONDS, 60, wakeup);

  while (1)
  {
    Dm.LedCtrl(GREEN_LED, ON);

    xTaskNotifyGive(handle_httpget_task);
    vTaskDelay(1);

#ifdef USE_DEEPSLEEP
    while (eTaskGetState(handle_httpget_task) == eTaskState::eRunning)
    {
      vTaskDelay(100);
      DEBUGPRNT.print(".");
    }

    float batt = Dm.readBatteryVoltage();
    DEBUGPRNT.println("### battery:" + String(batt, 1));

    client.stop();
    modemL.sleep();

    DEBUGPRNT.print("### going to Deepsleep...");
    long etime = millis() - elapstime;
    DEBUGPRNT.println(etime);

    DEBUGPRNT.flush();

    // Power off without modem.
    Dm.exUartPowerCtrl(OFF);
    Dm.exPowerCtrl(OFF);
    Dm.LedCtrl(GREEN_LED, OFF);

    Dm.sleep(SleepMode::DEEPSLEEP);

    elapstime = millis();
    DEBUGPRNT.println("### Wake up!!");
    modemL.wakeup();
    Dm.exPowerCtrl(ON);
    Dm.exUartPowerCtrl(ON);

    // must do again after sleep.
    DEBUGPRNT.println("###Start activate...");
    if (!modemL.activate(APN, USERNAME, PASSWORD))
    {
      DEBUGPRNT.println("###Error");
    }
    else
    {
      DEBUGPRNT.println("###OK\n");
    }
#else
    vTaskDelay(60000);
#endif
  }
  vTaskDelete(NULL);
}

void setup()
{
  DEBUGPRNT.begin(115200);

  vSetErrorSerial(&DEBUGPRNT);

  xTaskCreate(httpget, "httpget_task", 512, NULL, tskIDLE_PRIORITY + 4, &handle_httpget_task);

  xTaskCreate(main_task, "main_task", 512, NULL, tskIDLE_PRIORITY + 1, &Handle_main_task);

  vTaskStartScheduler();

  // error scheduler failed to start
  // should never get here
  while (1)
  {
    DEBUGPRNT.println("Scheduler Failed! \n");
    delay(1000);
  }
}

void loop()
{
  // never come here
}
