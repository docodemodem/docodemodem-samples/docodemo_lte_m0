//node http.js
const http = require('http');
const PORT = 3000;//change opend port number.

const server = http.createServer((req, res) => {
    if (req.url === '/' && req.method === 'GET') {
        console.log('GET')
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('Hello World!\n');
    }
});

server.listen(PORT);